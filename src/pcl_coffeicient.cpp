#include <iostream>
#include <ros/ros.h>
#include <pcl/pcl_base.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_cloud.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/voxel_grid.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/segmentation/extract_clusters.h>

//https://pointclouds.org/documentation/tutorials/extract_indices.html#extract-indices
//https://github.com/ori-drs/plane_seg
//#include <LinkPlot3DPlugin.hh>
ros::Publisher pub;

void 
cloud_cb (const sensor_msgs::PointCloud2ConstPtr& input)
{
  // Create a container for the data.
  sensor_msgs::PointCloud2 output;

  // Do data processing here...
  output = *input;//PointCloud2data
  // Convert the sensor_msgs/PointCloud2 data to pcl/PointCloud
  pcl::PointCloud<pcl::PointXYZ> cloud;
  pcl::PointCloud<pcl::PointXYZ> cloud_p;
  pcl::PointCloud<pcl::PointXYZ> cloud_f;
  pcl::fromROSMsg (*input, cloud);
  pcl::ModelCoefficients coefficients;
  pcl::PointIndices inliers;
  pcl_msgs::PointIndices ros_inliers;
  // Create the segmentation object
  pcl::SACSegmentation<pcl::PointXYZ> seg;
   // Optional
  seg.setOptimizeCoefficients (true);
  // Mandatory
  seg.setModelType (pcl::SACMODEL_PLANE);
  seg.setMethodType (pcl::SAC_RANSAC);
  seg.setDistanceThreshold (0.01);
  // To process multiple models, we run the process in a loop, and after each model is extracted, we go back to obtain the remaining points, and iterate. The inliers are obtained from the segmentation process, as follows:
  // seg.setInputCloud (cloud.makeShared ());
  // seg.segment (inliers, coefficients);
   // Publish the model coefficients
  seg.setInputCloud (cloud.makeShared ());
  seg.segment (inliers, coefficients);
  // pcl_conversions::fromPCL(inliers , ros_inliers);
  // pub.publish (ros_inliers);

  // pcl_msgs::ModelCoefficients ros_coefficients;
  // pcl_conversions::fromPCL(coefficients, ros_coefficients);//(ax+by+cz+d=0 graph)
 
  std::vector<pcl::PointIndices> cluster_indices;
  pcl::EuclideanClusterExtraction<pcl::PointXYZRGB> ec;
  pcl::exctract<pcl::PointXYZ> exctract;
  // specify euclidean cluster parameters
  ec.setClusterTolerance (0.02); // 2cm
  ec.setMinClusterSize (100);
  ec.setMaxClusterSize (25000);
  // ec.setIndices (inliers);
//   ec.setSearchMethod (tree);
  exctract.setInputCloud (cloud.makeShared ());
  // exctract the indices pertaining to each cluster and store in a vector of pcl::PointIndices
  ec.extract (cluster_indices);

  // declare an instance of the SegmentedClustersArray message
//   obj_recognition::SegmentedClustersArray CloudClusters;

  // declare the output variable instances
  sensor_msgs::PointCloud2 output_r;
  pcl::PCLPointCloud2 outputPCL;

  // here, cluster_indices is a vector of indices for each cluster. iterate through each indices object to work with them seporately
  for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it)
  {

    // create a new clusterData message object
    //obj_recognition::ClusterData clusterData;


    // create a pcl object to hold the extracted cluster
    pcl::PointCloud<pcl::PointXYZRGB> *cluster = new pcl::PointCloud<pcl::PointXYZRGB>;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr clusterPtr (cluster);

    // now we are in a vector of indices pertaining to a single cluster.
    // Assign each point corresponding to this cluster in xyzCloudPtrPassthroughFiltered a specific color for identification purposes
    for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); ++pit)
    {
      clusterPtr->points.push_back(inliers->points[*pit]);

        }


    // log the position of the cluster
    //clusterData.position[0] = (*cloudPtr).data[0];
    //clusterData.position[1] = (*cloudPtr).points.back().y;
    //clusterData.position[2] = (*cloudPtr).points.back().z;
    //std::string info_string = string(cloudPtr->points.back().x);
    //printf(clusterData.position[0]);

    // convert to pcl::PCLPointCloud2
    pcl::toPCLPointCloud2( *clusterPtr ,outputPCL);

    // Convert to ROS data type
    pcl_conversions::fromPCL(outputPCL, output_r);

    // add the cluster to the array message
    //clusterData.cluster = output;


  }

  // publish the clusters
  pub.publish(output_r);

}

 


int
main (int argc, char** argv)
{
  // Initialize ROS
  ros::init (argc, argv, "SeatDetect");
  ros::NodeHandle nh;

  // Create a ROS subscriber for the input point cloud
  ros::Subscriber sub = nh.subscribe ("/camera/depth/color/points", 1, cloud_cb);

  // Create a ROS publisher for the output point cloud
  //pub = nh.advertise<pcl_msgs::PointIndices>("/output/inliner", 1);
  pub = nh.advertise<sensor_msgs::PointCloud2> ("/output", 1);
  //pub = nh.advertise<pcl_msgs::ModelCoefficients> ("/output/coff", 1);
  // Spin
  ros::spin ();
}
