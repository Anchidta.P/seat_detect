#include <iostream>
#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/passthrough.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/segmentation/extract_clusters.h>


ros::Publisher pub;
void cloud_cb(const sensor_msgs::PointCloud2ConstPtr& cloud_msg)
{
   // Container for original & filtered data
  pcl::PCLPointCloud2* cloud = new pcl::PCLPointCloud2;
  pcl::PCLPointCloud2ConstPtr cloudPtr(cloud);
  pcl::PCLPointCloud2* cloud_filtered = new pcl::PCLPointCloud2;
  pcl::PCLPointCloud2Ptr cloudFilteredPtr (cloud_filtered);


  // Convert to PCL data type
  pcl_conversions::toPCL(*cloud_msg, *cloud);


  // Perform voxel grid downsampling filtering
  pcl::VoxelGrid<pcl::PCLPointCloud2> sor;
  sor.setInputCloud(cloudPtr);
  sor.setLeafSize(0.01, 0.01, 0.01);
  sor.filter(*cloudFilteredPtr);


  pcl::PointCloud<pcl::PointXYZRGB> *xyz_cloud = new pcl::PointCloud<pcl::PointXYZRGB>;
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr xyzCloudPtr (xyz_cloud); // need a boost shared pointer for pcl function inputs

  // convert the pcl::PointCloud2 tpye to pcl::PointCloud<pcl::PointXYZRGB>
  pcl::fromPCLPointCloud2(*cloudFilteredPtr, *xyzCloudPtr);


  //perform passthrough filtering to remove table leg

  // create a pcl object to hold the passthrough filtered results
  pcl::PointCloud<pcl::PointXYZRGB> *xyz_cloud_filtered = new pcl::PointCloud<pcl::PointXYZRGB>;
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr xyzCloudPtrFiltered (xyz_cloud_filtered);

   pcl::PointCloud<pcl::PointXYZRGB> *indices_xz = new pcl::PointCloud<pcl::PointXYZRGB>;
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr Indices_xz(indices_xz);
   pcl::PointCloud<pcl::PointXYZRGB> *cloud_out = new pcl::PointCloud<pcl::PointXYZRGB>;
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr Cloud_out(cloud_out);
  // Create the filtering object
  pcl::PassThrough<pcl::PointXYZRGB> pass  (true);
  pass.setInputCloud(xyzCloudPtr);
  pass.setFilterFieldName("y");
  pass.setFilterLimits(0.7,2);
  pass.setFilterLimitsNegative(true);
  
  // indices_x = pass.getRemovedIndices();

  // The indices_xz array indexes all points of cloud_in that have x between 0.0 and 1000.0 and z larger than 10.0 or smaller than -10.0

  pass.filter(*Cloud_out);
 // The resulting cloud_out contains all points of cloud_in that are finite and have:
 // x between 0.0 and 1000.0, z larger than 10.0 or smaller than -10.0 and intensity smaller than 0.5.

  // The indices_rem array indexes all points of cloud_in that have x smaller than 0.0 or larger than 1000.0
 // and also indexes all non-finite points of cloud_in
  // std::cerr << "Cloud after filtering: " << std::endl;
  // for (const auto& point: *xyzCloudPtrFiltered)
  //   std::cerr << "    " << point.x << " "
  //                       << point.y << " "
  //                       << point.z << std::endl;
  sensor_msgs::PointCloud2 output;
  pcl::PCLPointCloud2 outputPCL;
  pcl::toPCLPointCloud2( *Cloud_out ,outputPCL);

  // Convert to ROS data type
  pcl_conversions::fromPCL(outputPCL, output);
  pub.publish(output);




}
int
main (int argc, char** argv)
{
  // Initialize ROS
  ros::init (argc, argv, "obstacles");
  ros::NodeHandle nh;

  // Create a ROS subscriber for the input point cloud
  //ros::Subscriber sub = nh.subscribe ("/camera/depth/color/points", 1, cloud_cb);
  
  ros::Subscriber sub = nh.subscribe ("/r200/camera/depth_registered/points", 1, cloud_cb);

  // Create a ROS publisher for the output point cloud
  pub = nh.advertise<pcl::PCLPointCloud2>("obstacles", 1);

  // Spin
  ros::spin ();
}

