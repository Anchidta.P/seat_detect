### Seat_detect 


# Seat_Detect



realsense_gazebo_plugin from https://github.com/anchidta/realsense_gazebo_plugin

# How to use

terminal code

`$ roslaunch realsense_gazebo_plugin sofa_test.launch `  

#(launch realsence and sofa model)

`$ roslaunch realsense_gazebo_plugin depth_proc.launch `

`$ rosrun seat_detect pclplane`

#(for plane detect from pcl_inliner.cpp)

`$ rosrun seat_detect pcl2image`

#(for pcl to image from pcl2image.cpp)


End
